package com.app.henry.goldenbox.model.local
import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.app.henry.goldenbox.model.entity.Money

@Dao
interface MoneyDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(money: Money)

    @Query("select * from money order by id desc")
    fun all(): LiveData<List<Money>>

    @Query("select * from money where id = (:id)")
    fun get(id: Long): LiveData<Money>

    @Delete
    fun delete(money: Money)
}