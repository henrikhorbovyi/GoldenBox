package com.app.henry.goldenbox.view.moneys

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class HomeViewPagerAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {
    var titles = mutableListOf<String>()
    var fragments = mutableListOf<Fragment>()

    fun addFragment(title: String, fragment: Fragment){
        fragments.add(fragment)
        titles.add(title)
    }
    override fun getItem(position: Int): Fragment? = fragments[position]
    override fun getCount(): Int = fragments.count()
    override fun getPageTitle(position: Int): CharSequence = titles[position]
}