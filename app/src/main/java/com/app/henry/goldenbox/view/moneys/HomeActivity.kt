package com.app.henry.goldenbox.view.moneys

import android.content.Intent
import android.support.v4.view.ViewPager
import android.os.Bundle
import android.view.*
import android.widget.Toast
import com.app.henry.goldenbox.BaseActivity
import com.app.henry.goldenbox.R
import kotlinx.android.synthetic.main.activity_home.*
import com.app.henry.goldenbox.view.add_money.AddMoneyActivity

class HomeActivity: BaseActivity() {

    val datePagerAdapter = HomeViewPagerAdapter(supportFragmentManager)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val mViewPager = container
        setupViewPager(mViewPager)
        tabs.setupWithViewPager(mViewPager)
    }
    private fun setupViewPager(viewPager: ViewPager){

        datePagerAdapter.addFragment("Money", MoneyFragment())
        datePagerAdapter.addFragment("Stats", StatsFragment())
        viewPager.adapter = datePagerAdapter
    }
    fun addMoneyClicked(view: View) {
        startActivity(Intent(view.context, AddMoneyActivity::class.java))
        /*val registerForm = LayoutInflater.from(this@HomeActivity)
                .inflate(R.layout.activity_add_money,
                        window.decorView as ViewGroup,
                        false)

        val alert = AlertDialog.Builder(this@HomeActivity)
        alert.setView(registerForm)
                .setPositiveButton("Save", object : DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        Toast.makeText(applicationContext,"SAVED", Toast.LENGTH_LONG).show()
                    }
                })
                .show()*/
    }
    //MENU STUFF
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.action_settings -> Toast.makeText(this,"GO TO SETTINGS",Toast.LENGTH_LONG).show()
            R.id.by_date -> changeListOrder("DATE")
            R.id.by_price -> changeListOrder("PRICE")
            R.id.by_title -> changeListOrder("TITLE")
        }
        return super.onOptionsItemSelected(item)
    }

    private fun changeListOrder(order: String){
        val mf = datePagerAdapter.fragments.first() as MoneyFragment
        mf.orderListBy(order)
    }
}
