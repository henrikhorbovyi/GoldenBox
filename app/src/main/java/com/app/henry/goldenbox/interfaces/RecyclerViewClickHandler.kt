package com.app.henry.goldenbox.interfaces
import android.view.View

interface ItemClickListener{
    fun onItemClick(view: View, position: Int)
}
interface ItemLongClickListener{
    fun onItemLongClick(view: View, position: Int)
}