package com.app.henry.goldenbox

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar

open class BaseActivity : AppCompatActivity(){
    var toolbar : Toolbar? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onStart() {
        super.onStart()
        toolbarSetup()
    }
    open fun toolbarSetup() {
        toolbar = findViewById(R.id.toolbar)
        toolbar.apply {
            if (this != null)
                setSupportActionBar(this)
        }
    }
}