package com.app.henry.goldenbox.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.os.AsyncTask
import com.app.henry.goldenbox.model.entity.Money
import com.app.henry.goldenbox.model.local.AppDataBase
import com.app.henry.goldenbox.model.local.MoneyDao

/* TODO(APIS WILL BE CALLED HERE TOO, LATER) */
class MoneyRepository(context: Context){
    private var moneyDao: MoneyDao
    private var dataset: LiveData<List<Money>> = MutableLiveData<List<Money>>()

    init{
        val db = AppDataBase.getInstance(context)
        moneyDao = db!!.moneyDao()
        dataset = moneyDao.all()
    }

    fun save(money: Money) = SaveAsyncTask(moneyDao).execute(money)
    fun loadAll(): LiveData<List<Money>> = dataset
    fun delete(money: Money) = DeleteAsyncTask(moneyDao).execute(money)

    class SaveAsyncTask(private val mAsyncTaskDao: MoneyDao): AsyncTask<Money, Unit, Unit>(){
        override fun doInBackground(vararg params: Money) {
            mAsyncTaskDao.add(params[0])
        }
    }
    class DeleteAsyncTask(private val mAsyncTaskDao: MoneyDao): AsyncTask<Money, Unit, Unit>(){
        override fun doInBackground(vararg params: Money) {
            mAsyncTaskDao.delete(params[0])
        }
    }

}