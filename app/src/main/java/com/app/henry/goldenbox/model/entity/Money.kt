package com.app.henry.goldenbox.model.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.*

@Entity(tableName = "money")
class Money{
        @PrimaryKey(autoGenerate = true)
        var id: Long? = null
        @ColumnInfo(name = "title")
        var title: String? = null
        @ColumnInfo(name = "amount")
        var amount: Float? = null
        @ColumnInfo(name = "date")
        var date: Date? = null
        @ColumnInfo(name = "type")
        var type: Int? = null
        @ColumnInfo(name = "paid")
        var paid: Int? = null

    constructor(title: String, amount: Float, date: Date, type: Int, paid: Int){
        this.title = title
        this.amount = amount
        this.date = date
        this.type = type
        this.paid = paid
    }
    companion object{
        //paid
        val PAID = 1
        val NOT_PAID : Int = 0
        //type
        val INCOME : Int = 1
        val EXPENSE : Int = -1
    }
    /*It will be used someday */
    /*val categories : MutableList<String> = mutableListOf(
                "Food","Home Bills","Internet Shopping","Tax",
                "Clothes","Accessories","PLaces","Pets","Transport",
                "Trips", "Tickets", "Salary", "Borrowed Money", "Other")*/
}

