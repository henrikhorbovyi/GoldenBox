package com.app.henry.goldenbox.view.moneys
import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.Toast
import com.app.henry.goldenbox.R
import com.app.henry.goldenbox.model.entity.Money
import com.app.henry.goldenbox.interfaces.ItemClickListener
import com.app.henry.goldenbox.interfaces.ItemLongClickListener
import kotlinx.android.synthetic.main.fragment_money.*
import kotlinx.android.synthetic.main.money_item.view.*

class MoneyFragment: Fragment(), ItemClickListener, ItemLongClickListener  {

    private var moneyViewModel: MoneyViewModel? = null
    private val moneyAdapter: MoneyListAdapter by lazy { MoneyListAdapter(this, this) }
    private var moneyRecyclerView: RecyclerView? = null
    private val ITEM_MENU_OPTIONS = arrayOf("Edit Money", "Delete Money")
    private val ITEM_MENU_OPTION_UPDATE = 0
    private val ITEM_MENU_OPTION_DELETE = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_money, container, false)
        moneyViewModel = ViewModelProviders.of(this).get(MoneyViewModel::class.java)
        moneyRecyclerView = view.findViewById(R.id.moneyRecyclerView)
        setUpRecyclerView()
        moneyViewModel!!.moneys().observe(this, Observer {
            it.let { list ->
                if(list != null){
                    setDataItems(list)
                    emptyListMessage.visibility = if(list.isEmpty()) View.VISIBLE else View.GONE
                }
            }
        })
        return view
    }

    private fun setUpRecyclerView() {
        moneyRecyclerView?.layoutManager = LinearLayoutManager(context)
        moneyRecyclerView?.adapter = moneyAdapter
        moneyRecyclerView?.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    }
    private fun setDataItems(data: List<Money>) = moneyAdapter.update(data.toMutableList())

    override fun onItemClick(view: View, position: Int) {
        Toast.makeText(context, "$position of the item", Toast.LENGTH_LONG).show()
    }
    override fun onItemLongClick(view: View, position: Int) {
        AlertDialog.Builder(context)
                .setItems(ITEM_MENU_OPTIONS, { _: DialogInterface, item: Int ->
                    when(item){
                        ITEM_MENU_OPTION_UPDATE -> Toast.makeText(context, "Change to update screen!", Toast.LENGTH_LONG).show()
                        ITEM_MENU_OPTION_DELETE -> moneyViewModel?.delete(moneyAdapter.get(position))//DeleteTask(this, viewAdapter?.get(position), position).execute()
                    }
                }).show()
    }

    fun orderListBy(order: String){
        val dataset = moneyAdapter.dataset
        var ordered = mutableListOf<Money>()

        ordered = dataset.sortedWith(compareBy(Money::date)).toMutableList()


        when(order){
            "PRICE" -> ordered = dataset.sortedWith(compareBy(Money::amount)).toMutableList()
            "TITLE" -> ordered = dataset.sortedWith(compareBy(Money::title)).toMutableList()
        }
        moneyAdapter.update(ordered)
    }
}