package com.app.henry.goldenbox.model.local
import android.arch.persistence.room.*
import android.content.Context
import com.app.henry.goldenbox.model.entity.Money

@Database(entities = [(Money::class)], version = 2)
@TypeConverters(RoomTypeConverters::class)
abstract class AppDataBase: RoomDatabase(){
    abstract fun moneyDao(): MoneyDao

    companion object {
        var INSTANCE : AppDataBase? = null
        val DB_NAME  : String       = "app.db"

        fun getInstance(context: Context) : AppDataBase?{
            if(INSTANCE == null){
                synchronized(AppDataBase::class.java){
                    INSTANCE = Room
                            .databaseBuilder(
                                    context.applicationContext,
                                    AppDataBase::class.java,
                                    DB_NAME)
                            /*.allowMainThreadQueries()*/
                            .fallbackToDestructiveMigration()
                            .build()
                }
            }
            return INSTANCE
        }
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}