package com.app.henry.goldenbox.view.moneys
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.henry.goldenbox.R
import com.app.henry.goldenbox.model.entity.Money
import com.app.henry.goldenbox.interfaces.ItemClickListener
import com.app.henry.goldenbox.interfaces.ItemLongClickListener

class MoneyListAdapter(val clickListener: ItemClickListener, val longClickListener: ItemLongClickListener):
        RecyclerView.Adapter<MoneyListViewHolder>() {

    var dataset = mutableListOf<Money>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoneyListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.money_item, parent, false)
        return MoneyListViewHolder(view, clickListener, longClickListener)
    }
    override fun getItemCount(): Int = dataset.count()
    override fun onBindViewHolder(holder: MoneyListViewHolder, position: Int) {
        holder.bind(dataset[position])
    }

    fun get(position: Int) = dataset[position]
    fun update(dataset: MutableList<Money>){
        this.dataset.clear()
        this.dataset.addAll(dataset)
        notifyDataSetChanged()
    }
}